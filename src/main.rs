#![feature(test)]

pub mod lzw;

use crate::lzw::*;

use std::fs::{read, write, File};
use std::io::{BufReader, Cursor, Read};
use std::iter;
use std::path::PathBuf;
use std::time::Instant;

use structopt::StructOpt;

#[derive(StructOpt, Clone)]
struct Common {
    /// Input file
    #[structopt(parse(from_os_str))]
    pub input: PathBuf,

    /// Output file
    #[structopt(parse(from_os_str), default_value = "./out")]
    pub output: PathBuf,

    /// Will not load entire file into ram first when set
    #[structopt(short, long)]
    pub no_ram: bool,
}

#[derive(StructOpt)]
#[structopt(about = "Compress or decompress a file using LZW compression of 12- or 16-bit encodings.")]
enum CompressSettings {
    /// Encodes the input file using LZW compression of various varieties
    Encode {
        #[structopt(flatten)]
        common: Common,

        /// Forces the compressor to use 12-bit compression (or dynamic if -s set also)
        #[structopt(short)]
        twelve: bool,

        /// Forces the compressor to use 16-bit compression (or dynamic if -t set also)
        #[structopt(short)]
        sixteen: bool,
    },

    /// Decodes an LZW compressed input file
    Decode {
        #[structopt(flatten)]
        common: Common,
    },
}

const RAM_MIN_CAPACITY: usize = 10_000_000;

// Creates a faster iterator over a file that loads in chunks of bytes at a time.
pub fn create_input_iter(in_file: File, buffer: usize) -> impl Iterator<Item = u8> {
    let mut in_file = in_file;
    let repeater = iter::repeat_with(move || {
        let mut bytes: Vec<u8> = vec![0u8; buffer];
        let size = in_file
            .read(&mut bytes[..])
            .expect("Error reading from input file");
        if size < bytes.len() {
            bytes.resize(size, 0);
        }
        bytes
    });
    let unflattened = repeater.take_while(|v| !v.is_empty());
    let flattened = unflattened.flatten();
    flattened
}

const TWE_LIM: usize = 10_000;
const DYN_LIM: usize = 1_000_000;

fn main() {
    let settings = CompressSettings::from_args();
    match settings {
        CompressSettings::Encode {
            twelve,
            sixteen,
            common,
            ..
        } => {
            let (time, file_size, enc_size) = if !common.no_ram {
                println!("Loading into ram");
                let start = Instant::now();
                let data = read(common.input).expect("Unable to read input file");
                let file_size = data.len();

                // Encoding with in-ram vectors

                // Picks the encoding strategy given the user inputs.
                let strat = match (twelve, sixteen) {
                    (false, false) => {
                        if file_size < TWE_LIM {
                            Strat::Twelve
                        } else if file_size < DYN_LIM {
                            Strat::Dynamic
                        } else {
                            Strat::Short
                        }
                    }
                    (true, false) => Strat::Twelve,
                    (false, true) => Strat::Short,
                    (true, true) => Strat::Dynamic,
                };

                let encoded =
                    full_compress(data.iter().cloned(), Vec::new(), strat).expect("Encoding error");

                write(common.output, &encoded).expect("Unable to write to output file");

                let time = start.elapsed();

                let enc_size = encoded.len();
                (time, file_size, enc_size)
            } else {
                let in_file = File::open(common.input).expect("Unable to read input file");
                let file_size = in_file
                    .metadata()
                    .expect("Unable to read input file metadata")
                    .len() as usize;

                let out_file = File::create(common.output).expect("Unable to open output file");

                let start = Instant::now();

                // Encoding; reading and writing files at the same time.

                // Picks the encoding strategy given the user inputs.
                let strat = match (twelve, sixteen) {
                    (false, false) => {
                        if file_size < TWE_LIM {
                            Strat::Twelve
                        } else if file_size < DYN_LIM {
                            Strat::Dynamic
                        } else {
                            Strat::Short
                        }
                    }
                    (true, false) => Strat::Twelve,
                    (false, true) => Strat::Short,
                    (true, true) => Strat::Dynamic,
                };

                let out_file = full_compress(
                    create_input_iter(in_file, RAM_MIN_CAPACITY),
                    out_file,
                    strat,
                )
                .expect("Truncating failure or unable to write to output file");
                let enc_size = out_file
                    .metadata()
                    .expect("Unable to read output file metadata")
                    .len() as usize;

                let time = start.elapsed();
                (time, file_size, enc_size)
            };

            println!("Input Size: {} B", file_size);
            println!("Encoded Size: {} B", enc_size);
            println!("Time: {} ms", time.as_millis());
            println!(
                "Compression Factor: {:.3} x",
                file_size as f64 / ((enc_size + 1) as f64)
            );
        }

        CompressSettings::Decode { common, .. } => {
            let (time, file_size, enc_size) = if !common.no_ram {
                println!("Loading into ram");
                let start = Instant::now();
                let data = read(common.input).expect("Unable to read input file");
                let enc_size = data.len();

                // Decoding with ram vectors

                let mut decoded = Vec::new();
                full_decompress(Cursor::new(data), &mut decoded).expect("Decoding error");

                write(common.output, &decoded).expect("Unable to write to output file");

                let time = start.elapsed();

                let file_size = decoded.len();
                (time, file_size, enc_size)
            } else {
                let in_file = File::open(common.input).expect("Unable to read input file");
                let enc_size = in_file
                    .metadata()
                    .expect("Unable to read input file metadata")
                    .len() as usize;

                let mut out_file = File::create(common.output).expect("Unable to open output file");

                let start = Instant::now();

                // Encoding; reading and writing files at the same time.

                full_decompress(
                    BufReader::with_capacity(RAM_MIN_CAPACITY, in_file),
                    &mut out_file,
                )
                .expect("Truncating failure or unable to write to output file");
                let file_size = out_file
                    .metadata()
                    .expect("Unable to read output file metadata")
                    .len() as usize;

                let time = start.elapsed();
                (time, file_size, enc_size)
            };

            println!("Encoded Size: {} B", enc_size);
            println!("Output Size: {} B", file_size);
            println!("Time: {} ms", time.as_millis());
            println!(
                "Compression Factor: {:.3} x",
                file_size as f64 / (enc_size as f64)
            );
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    use std::fs::{read, File};

    /// Tests that the file-to-iterator function works as intended.
    #[test]
    fn file_iter_test() {
        let vec = read("texts/Celsissimus.txt").expect("Unable to read input file");
        let mut vec_iter = vec.iter().cloned();
        let in_file = File::open("texts/Celsissimus.txt").expect("Unable to read input file");
        let mut file_iter = create_input_iter(in_file, 1_000);
        let mut count = 0;
        loop {
            let viv = vec_iter.next();
            let fiv = file_iter.next();
            assert_eq!(viv, fiv, "Iterators do not match at index {}", count);
            count += 1;
            if viv.is_none() {
                break;
            }
        }
    }
}
