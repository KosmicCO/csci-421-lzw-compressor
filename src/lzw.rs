use std::collections::HashMap;
use std::io::{Read, Write};

/// Enum of different encoding strategies that the compressor can use (pick from 12-bit, 16-bit, or dynamic-bit encoding).
pub enum Strat {
    Twelve,
    Short,
    Dynamic,
}

/// Compression algorithm which supports all the nice features.
pub fn full_compress<I: Iterator<Item = u8>, W: Write>(
    input: I,
    output: W,
    strat: Strat,
) -> std::io::Result<W> {
    let mut output = output;
    match strat {
        Strat::Twelve => {
            output.write(&[0u8])?; // signals using lzw-twelve
            println!("Using twelve-bit compression");
            let mut trunc = TwelveTruncate::new(output);
            lzw_compress(input, &mut trunc)?;
            trunc.finish()
        }
        Strat::Short => {
            output.write(&[1u8])?; // signals using lzw-short
            println!("Using sixteen-bit compression");
            let mut trunc = ShortTruncate(output);
            lzw_compress(input, &mut trunc)?;
            Ok(trunc.0)
        }
        Strat::Dynamic => {
            output.write(&[2u8])?; // signals using lzw-dynamic
            println!("Using dynamic-bit compression");
            let mut trunc = DynTruncate::new(output);
            lzw_compress(input, &mut trunc)?;
            trunc.finish()
        }
    }
}

/// Decompression algorithm which supports all the nice features automatically.
pub fn full_decompress(input: impl Read, output: &mut impl Write) -> std::io::Result<()> {
    let mut input = input;
    let mut select = [0u8];
    input.read(&mut select)?;
    match select[0] {
        0u8 => {
            println!("Using twelve-bit decompression");
            let expnd = TwelveExpand::new(input);
            lzw_decompress(expnd, output)
        }
        1u8 => {
            println!("Using sixteen-bit decompression");
            let expnd = ShortExpand(input);
            lzw_decompress(expnd, output)
        }
        2u8 => {
            println!("Using dynamic-bit decompression");
            let expnd = DynExpand::new(input);
            lzw_decompress(expnd, output)
        }
        _ => Err(std::io::Error::new(
            std::io::ErrorKind::InvalidInput,
            "file has invalid encoding byte",
        )),
    }
}

/// Takes the codes from the `LZWCompressor` as input and truncates them to fit neatly into bytes.
/// Can also automatically write to a file instead of outputting the bits at the end, etc.
pub trait Truncator {
    /// Pushes a code from the compressor to the truncator.
    fn push(&mut self, code: u16) -> std::io::Result<()>;

    /// Returns the max code that is supported by the truncator.
    fn max() -> u16;
}

/// Compresses the input using lzw compression.
pub fn lzw_compress<I: Iterator<Item = u8>, T: Truncator>(
    input: I,
    trunc: &mut T,
) -> std::io::Result<()> {
    let mut codes: HashMap<(u16, u8), u16> = HashMap::new();
    let mut next_code = 255;
    let max = T::max();
    let mut input = input;

    let mut next = {
        if let Some(next) = input.next() {
            next
        } else {
            return Ok(());
        }
    };

    let mut code: u16 = next as u16;

    loop {
        // Next character
        if let Some(nnext) = input.next() {
            // TODO: Compiler directive here if too slow
            next = nnext.clone()
        } else {
            trunc.push(code)?;
            break;
        }

        // Code update
        if let Some(ncode) = codes.get(&(code, next)) {
            // The code plus the symbol was in the map
            code = ncode.clone();
        } else {
            // The code plus the symbol was not in the map
            if next_code <= max - 1 {
                next_code += 1;
                // Add the new code to the map if there is room
                codes.insert((code, next), next_code);
            }
            trunc.push(code)?;
            code = next as u16;
        }
    }

    Ok(())
}

/// Takes the bytes from the compressed file and expands to `u16` codes.
/// Can also automatically read from a file instead of inputting the codes at the beginning, etc.
pub trait Expander {
    /// Gets the next expanded code.
    fn next_code(&mut self) -> std::io::Result<Option<u16>>;

    /// Returns the max code that is supported by the Expander.
    fn max() -> u16;
}

/// Decrompresses from normal LZW.
pub fn lzw_decompress<E: Expander>(expnd: E, write: &mut impl Write) -> std::io::Result<()> {
    let mut codes: Vec<Vec<u8>> = (0u8..=255u8).map(|b| vec![b]).collect();
    let max = E::max();
    let mut expnd = expnd;

    let mut prev_code = {
        if let Some(code) = expnd.next_code()? {
            code
        } else {
            return Ok(());
        }
    };

    assert!(prev_code < 256); // initial input is not valid

    write.write(&codes[prev_code as usize])?;

    let mut code = {
        if let Some(code) = expnd.next_code()? {
            code
        } else {
            return Ok(());
        }
    };

    loop {
        let add = if let Some(bytes) = codes.get(code as usize) {
            write.write(bytes)?;
            let ret = if codes.len() <= max as usize {
                let mut nvec = codes[prev_code as usize].clone();
                nvec.push(bytes[0]);
                Some(nvec)
            } else {
                None
            };
            ret
        } else {
            assert_eq!(code as usize, codes.len());
            let old_vec = &codes[prev_code as usize];
            let mut nvec = old_vec.clone();
            nvec.push(old_vec[0]);
            write.write(&nvec[..])?;
            Some(nvec)
        };

        if let Some(bytes) = add {
            codes.push(bytes);
        }

        if let Some(ncode) = expnd.next_code()? {
            prev_code = code;
            code = ncode;
        } else {
            break;
        }
    }

    Ok(())
}

/// Truncates to 12-bit codes.
pub struct TwelveTruncate<W: Write> {
    out: W,
    buffer: u32,
    even: bool,
}

impl<W: Write> TwelveTruncate<W> {
    /// Creates a new instance.
    pub fn new(out: W) -> Self {
        Self {
            out,
            buffer: 0,
            even: false,
        }
    }

    /// Finishes writting bytes and outputs internal [`Write`].
    pub fn finish(self) -> std::io::Result<W> {
        let (mut out, even, buffer) = match self {
            Self { out, even, buffer } => (out, even, buffer),
        };
        if even {
            let bytes = buffer.to_be_bytes();
            out.write(&bytes[1..3])?;
        }
        Ok(out)
    }
}

impl<W: Write> Truncator for TwelveTruncate<W> {
    fn push(&mut self, code: u16) -> std::io::Result<()> {
        if self.even {
            // Put into the buffer even part: ---- ---- --XX XXXX
            let buffer = self.buffer | (0x0000_0FFF & code as u32);
            let bytes = buffer.to_be_bytes();
            self.out.write(&bytes[1..4])?;
        } else {
            // Put into the buffer odd part: ---- XXXX XX-- ----
            self.buffer = (code as u32) << 12;
        }
        self.even = !self.even;
        Ok(())
    }

    fn max() -> u16 {
        0x0FFF
    }
}

/// Expands from 12-bit codes.
pub struct TwelveExpand<R: Read> {
    input: R,
    buffer: [u8; 3],
    extra: bool, // Whether there is another value in self.buffer
}

impl<R: Read> TwelveExpand<R> {
    /// Creates a new instance.
    pub fn new(input: R) -> Self {
        Self {
            input,
            buffer: [0; 3],
            extra: false,
        }
    }
}

impl<R: Read> Expander for TwelveExpand<R> {
    fn next_code(&mut self) -> std::io::Result<Option<u16>> {
        if self.extra {
            let out = (0x0F & self.buffer[1] as u16) << 8 | (self.buffer[2] as u16);
            self.extra = false;
            Ok(Some(out))
        } else {
            let bytes_num = self.input.read(&mut self.buffer)?;
            if bytes_num < 2 {
                return Ok(None);
            }
            self.extra = bytes_num >= 3;
            let out = (self.buffer[0] as u16) << 4 | (self.buffer[1] as u16) >> 4;
            Ok(Some(out))
        }
    }

    fn max() -> u16 {
        0x0FFF
    }
}

/// Truncates to 16-bit codes (no truncation).
pub struct ShortTruncate<W: Write>(W);

impl<W: Write> Truncator for ShortTruncate<W> {
    fn push(&mut self, code: u16) -> std::io::Result<()> {
        let bytes = code.to_be_bytes();
        self.0.write(&bytes)?;
        Ok(())
    }

    fn max() -> u16 {
        0xFFFF
    }
}

/// Expands from 16-bit codes (no expansion).
pub struct ShortExpand<R: Read>(R);

impl<R: Read> Expander for ShortExpand<R> {
    fn next_code(&mut self) -> std::io::Result<Option<u16>> {
        let mut bytes = [0; 2];
        let bytes_num = self.0.read(&mut bytes)?;
        if bytes_num < 2 {
            Ok(None)
        } else {
            Ok(Some(u16::from_be_bytes(bytes)))
        }
    }

    fn max() -> u16 {
        0xFFFF
    }
}

/// A truncator which first encodes using 12-bit codes and then switches to 16-bit codes when it reaches the max.
/// Should save about 50KB for files longer than 100KB.
pub struct DynTruncate<W: Write> {
    output: W,
    buffer: u32,
    even: bool,
    sixteen: bool,
}

impl<W: Write> DynTruncate<W> {
    /// Creates a new instance.
    pub fn new(output: W) -> Self {
        Self {
            output,
            buffer: 0,
            even: false,
            sixteen: false,
        }
    }

    /// Finishes writting bytes and outputs internal [`Write`].
    pub fn finish(self) -> std::io::Result<W> {
        let (sixteen, mut input, even, buffer) = match self {
            Self {
                sixteen,
                output: input,
                even,
                buffer,
                ..
            } => (sixteen, input, even, buffer),
        };
        if !sixteen {
            if even {
                let bytes = buffer.to_be_bytes();
                input.write(&bytes[1..3])?;
            }
        }
        Ok(input)
    }
}

impl<W: Write> Truncator for DynTruncate<W> {
    fn push(&mut self, code: u16) -> std::io::Result<()> {
        if self.sixteen {
            let bytes = code.to_be_bytes();
            self.output.write(&bytes)?;
            Ok(())
        } else {
            if code >= 0x0FFE {
                // Need to convert whatever we had to 16-bit encoding.
                if self.even {
                    self.buffer <<= 4;
                    self.buffer |= code as u32;
                    self.output.write(&[0xFF, 0xFF, 0xFF])?;
                    self.output.write(&self.buffer.to_be_bytes())?;
                } else {
                    let bytes = code.to_be_bytes();
                    self.output.write(&[0xFF, 0xFF, 0xFF])?;
                    self.output.write(&bytes)?;
                }
                self.sixteen = true;
                Ok(())
            } else {
                if self.even {
                    // Put into the buffer even part: ---- ---- --XX XXXX
                    let buffer = self.buffer | (0x0000_0FFF & code as u32);
                    let bytes = buffer.to_be_bytes();
                    self.output.write(&bytes[1..4])?;
                } else {
                    // Put into the buffer odd part: ---- XXXX XX-- ----
                    self.buffer = (code as u32) << 12;
                }
                self.even = !self.even;
                Ok(())
            }
        }
    }

    fn max() -> u16 {
        0xFFFF
    }
}

/// Expands from dynamically truncated codes.
pub struct DynExpand<R: Read> {
    input: R,
    buffer: [u8; 3],
    extra: bool, // Whether there is another value in self.buffer
    sixteen: bool,
}

impl<R: Read> DynExpand<R> {
    /// Creates a new instance.
    pub fn new(input: R) -> Self {
        Self {
            input,
            buffer: [0; 3],
            extra: false,
            sixteen: false,
        }
    }
}

impl<R: Read> Expander for DynExpand<R> {
    fn next_code(&mut self) -> std::io::Result<Option<u16>> {
        if !self.sixteen {
            // 12-bit encoding
            if self.extra {
                let out = (0x0F & self.buffer[1] as u16) << 8 | (self.buffer[2] as u16);
                self.extra = false;
                return Ok(Some(out));
            } else {
                let bytes_num = self.input.read(&mut self.buffer)?;
                if bytes_num < 2 {
                    return Ok(None);
                }
                self.extra = bytes_num >= 3;
                if self.extra && self.buffer == [0xFF, 0xFF, 0xFF] {
                    // We've seen the marker that says to switch to 16-bit.
                    self.sixteen = true;
                } else {
                    let out = (self.buffer[0] as u16) << 4 | (self.buffer[1] as u16) >> 4;
                    return Ok(Some(out));
                }
            }
        }

        // 16-bit encoding
        let mut bytes = [0; 2];
        let bytes_num = self.input.read(&mut bytes)?;
        if bytes_num < 2 {
            Ok(None)
        } else {
            Ok(Some(u16::from_be_bytes(bytes)))
        }
    }

    fn max() -> u16 {
        0xFFFF
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    extern crate test;

    use std::fs::read;
    use std::io::Cursor;
    use test::{black_box, Bencher};

    struct VecTruncator(Vec<u16>);

    impl Truncator for VecTruncator {
        fn push(&mut self, code: u16) -> std::io::Result<()> {
            self.0.push(code);
            Ok(())
        }

        fn max() -> u16 {
            u16::MAX
        }
    }

    struct VecExpander(Vec<u16>, usize);

    impl Expander for VecExpander {
        fn next_code(&mut self) -> std::io::Result<Option<u16>> {
            if self.1 < self.0.len() {
                let v = self.0[self.1];
                self.1 += 1;
                return Ok(Some(v));
            } else {
                Ok(None)
            }
        }

        fn max() -> u16 {
            return u16::MAX;
        }
    }

    /// Tests that encoding/decoding no bytes works.
    #[test]
    fn encode_decode_empty() {
        let celsissimus = vec![];
        let mut trunc = VecTruncator(Vec::new());
        lzw_compress(celsissimus.iter().cloned(), &mut trunc).unwrap();
        let expnd = VecExpander(trunc.0, 0);
        let mut out = Vec::new();
        lzw_decompress(expnd, &mut out).unwrap();
        assert_eq!(celsissimus, out);
    }

    /// Tests that encoding/decoding a single byte works.
    #[test]
    fn encode_decode_single() {
        let celsissimus = vec![2u8];
        let mut trunc = VecTruncator(Vec::new());
        lzw_compress(celsissimus.iter().cloned(), &mut trunc).unwrap();
        let expnd = VecExpander(trunc.0, 0);
        let mut out = Vec::new();
        lzw_decompress(expnd, &mut out).unwrap();
        assert_eq!(celsissimus, out);
    }

    /// Tests that encoding/decoding two bytes works.
    #[test]
    fn encode_decode_double() {
        let celsissimus = vec![2u8, 87u8];
        let mut trunc = VecTruncator(Vec::new());
        lzw_compress(celsissimus.iter().cloned(), &mut trunc).unwrap();
        let expnd = VecExpander(trunc.0, 0);
        let mut out = Vec::new();
        lzw_decompress(expnd, &mut out).unwrap();
        assert_eq!(celsissimus, out);
    }

    /// Tests that encoding/decoding a large file (even in german) works.
    #[test]
    fn encode_decode_celsissimus() {
        let celsissimus = read("texts/Celsissimus.txt").expect("error reading in test document");
        let mut trunc = VecTruncator(Vec::new());
        lzw_compress(celsissimus.iter().cloned(), &mut trunc).unwrap();
        let expnd = VecExpander(trunc.0, 0);
        let mut out = Vec::new();
        lzw_decompress(expnd, &mut out).unwrap();
        assert_eq!(celsissimus, out);
    }

    /// Tests the speed of cloning an interator.
    #[bench]
    fn iter_clone_benchmark(b: &mut Bencher) {
        let celsissimus = read("texts/Celsissimus.txt").expect("error reading in test document");
        b.iter(move || black_box(celsissimus.iter().cloned().next()));
    }

    /// Tests the speed of encoding/decoding a large file.
    #[bench]
    fn encoding_celsissimus_benchmark(b: &mut Bencher) {
        let celsissimus = read("texts/Celsissimus.txt").expect("error reading in test document");
        b.iter(move || lzw_compress(celsissimus.iter().cloned(), &mut VecTruncator(Vec::new())));
    }

    /// Tests that expanding/truncading a large file works.
    /// The middle vector is for debugging.
    #[test]
    fn twelve_celsissimus_expand_truncate() {
        let celsissimus = read("texts/Celsissimus.txt").expect("error reading in test document");
        let mut expand = TwelveExpand::new(&celsissimus[..]);
        let mut truncate = TwelveTruncate::new(Vec::new());
        let mut middle = Vec::new();
        loop {
            if let Some(code) = expand.next_code().expect("next_code returned an error") {
                middle.push(code);
            } else {
                break;
            }
        }

        for code in middle {
            truncate.push(code).expect("push returned an error");
        }
        let out = truncate.finish().expect("finish returned an error");
        let ldiff = celsissimus.len() - out.len(); // will panic if is negative, which is good
        assert!(ldiff <= 1);
        assert_eq!(celsissimus[..out.len() - 1], out[..out.len() - 1]);
    }

    /// Tests that encoding/decoding with trucating/expanding in between works.
    #[test]
    fn twelve_encode_truncate_expand_decode() {
        let celsissimus = read("texts/Celsissimus.txt").expect("error reading in test document");
        let mut trunc = TwelveTruncate::new(Vec::new());
        lzw_compress(celsissimus.iter().cloned(), &mut trunc).unwrap();
        let read = Cursor::new(trunc.finish().expect("finish returned an error"));
        let expnd = TwelveExpand::new(read);
        let mut out = Vec::new();
        lzw_decompress(expnd, &mut out).unwrap();
        assert_eq!(celsissimus, out);
    }

    /// Tests that expanding/truncading a large file works.
    /// The middle vector is for debugging.
    #[test]
    fn short_celsissimus_expand_truncate() {
        let celsissimus = read("texts/Celsissimus.txt").expect("error reading in test document");
        let mut expand = ShortExpand(&celsissimus[..]);
        let mut truncate = ShortTruncate(Vec::new());
        let mut middle = Vec::new();
        loop {
            if let Some(code) = expand.next_code().expect("next_code returned an error") {
                middle.push(code);
            } else {
                break;
            }
        }

        for code in middle {
            truncate.push(code).expect("push returned an error");
        }
        let out = truncate.0;
        let ldiff = celsissimus.len() - out.len(); // will panic if is negative, which is good
        assert!(ldiff <= 1);
        assert_eq!(celsissimus[..out.len() - 1], out[..out.len() - 1]);
    }

    /// Tests that encoding/decoding with trucating/expanding in between works.
    #[test]
    fn short_encode_truncate_expand_decode() {
        let celsissimus = read("texts/Celsissimus.txt").expect("error reading in test document");
        let mut trunc = ShortTruncate(Vec::new());
        lzw_compress(celsissimus.iter().cloned(), &mut trunc).unwrap();
        let read = Cursor::new(trunc.0);
        let expnd = ShortExpand(read);
        let mut out = Vec::new();
        lzw_decompress(expnd, &mut out).unwrap();
        assert_eq!(celsissimus, out);
    }

    /// Tests that expanding/truncading a large file works.
    /// The middle vector is for debugging.
    #[test]
    fn dyn_small_truncate_expand() {
        let codes = vec![1, 2, 3, 7, 0x0FFC, 4, 5, 6, 0xFFFC];
        let mut trunc = DynTruncate::new(Vec::new());
        for c in codes.clone() {
            trunc.push(c).expect("push errored");
        }

        let bytes = trunc.finish().expect("finish errored");
        let mut expnd = DynExpand::new(Cursor::new(bytes));

        let mut out_codes = Vec::new();

        loop {
            let nc = expnd.next_code().expect("next_code errored");
            if nc.is_none() {
                break;
            }
            out_codes.push(nc.unwrap());
        }
        assert_eq!(codes, out_codes);
    }

    /// Tests that encoding/decoding with trucating/expanding with the `Dyn` variety in between works.
    #[test]
    fn dyn_encode_truncate_expand_decode() {
        let mut celsissimus =
            read("texts/Celsissimus.txt").expect("error reading in test document");
        celsissimus.resize(10066, 0);
        let mut mid_trunc = VecTruncator(Vec::new());
        lzw_compress(celsissimus.iter().cloned(), &mut mid_trunc).unwrap();

        let mut trunc = DynTruncate::new(Vec::new());

        // middle truncators and expandors for debug purposes
        for c in mid_trunc.0.clone() {
            trunc.push(c).expect("push errored");
        }

        let read = Cursor::new(trunc.finish().expect("finish errored"));
        let mut expnd = DynExpand::new(read);

        let mut exvec = Vec::new();

        loop {
            let nc = expnd.next_code().expect("next_code errored");
            if nc.is_none() {
                break;
            }
            exvec.push(nc.unwrap());
        }

        for i in 0..exvec.len() {
            let n1 = mid_trunc.0[i];
            let n2 = exvec[i];
            assert_eq!(n1, n2, "codes not equal at index {}", i);
        }

        let mid_expnd = VecExpander(exvec, 0);
        let mut out = Vec::new();
        lzw_decompress(mid_expnd, &mut out).unwrap();
        for i in 0..celsissimus.len() {
            let h1 = celsissimus[i];
            let h2 = out[i];
            assert_eq!(h1, h2, "output not equal at index {}", i);
        }
        assert_eq!(celsissimus, out);
    }
}
