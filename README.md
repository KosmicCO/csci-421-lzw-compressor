# LZW 12- and 16-bit Encoding Compressor

The compressor `compress` is based off of an LZW comrpessor that encodes using 12- and/or 16-bit output, with the option to use a dynamic encoding, which uses 12-bits for as long as possible until moving to 16-bit encoding.
Documentation of the functions used in the program can be found in the source or in the generated documentation found at `doc/csci_421_lzw_compressor/index.html`.

Texts used to test originally from these sites:
- http://mattmahoney.net/dc/textdata.html
- https://www.learningcontainer.com/sample-text-file/

See [repo](https://gitlab.com/KosmicCO/csci-421-lzw-compressor/) for complete list of test files.

## Use

For basic compression of a file, the filename can simply be provided along with the `encode` directive.
```
./compress encode my_file.txt
```

Similarly, an output file name can be provided as well.
```
./compress encode my_file.txt my_file.cmp
```

The program will attempt to choose an encoding best suited for the file, (between 12-bit, 16-bit, and dynamic encoding).
The program will print which encoding method is used.
The encoding method can be specified manually with the `-t` and `-s` flags.
The `-t` flag specifies 12-bit encoding while the `-s` flag specifies 16-bit encoding.
The presence of both flags indicates that dynamic encoding should be used.
```
./compress encode my_file.txt my_file.cmp -t
./compress encode my_file.txt my_file.cmp -s
./compress encode my_file.txt my_file.cmp -t -s
```

Similarly, files can be decompressed using the `decode` directive.
```
./compress decode my_file.cmp
```

Likewise, an output file might be specified.
```
./compress decode my_file.cmp my_file.txt
```

Note that the decoder will automatically determine which bit-encoding was used to encode the text originally.

For either encoding or decoding, an optional flag `-n` can be passed that limits the ram usage as well; though encoding/decoding will be much slower, since it has to poll memory more often (and also is unoptimized).
In general, it is suggested to use the default method, only using the ram-limiting version when ram is of serious consideration (multi-GB files).

## Compression Rates

Images of the compression rates/factors and times are provided with example files from the [Canterbury Courpus](https://corpus.canterbury.ac.nz/descriptions/) from the headers
- Canterbury
- Artificial
- Large
  
under the `images` folder.
Some examples follow, though the folder contains many more example compressions.

![](images/canterbury/alice29.png)
![](images/artificial/aaa.png)
![](images/large/bible.png)
